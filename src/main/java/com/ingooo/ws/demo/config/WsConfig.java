package com.ingooo.ws.demo.config;

import com.ingooo.ws.util.listener.RequestListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * Create by 丶TheEnd on 2019/10/25 0025.
 * @author Administrator
 */
@Configuration
public class WsConfig {

    @Bean
    public RequestListener getRequestListener(){
        return new RequestListener();
    }

    /**
     * 把自定义 Listener 添加到 IOC 容器
     * @return
     */
    @Bean
    public ServletListenerRegistrationBean<RequestListener> servletListenerRegistrationBean(@Autowired RequestListener requestListener) {
        ServletListenerRegistrationBean<RequestListener> servletListenerRegistrationBean = new ServletListenerRegistrationBean<>();
        servletListenerRegistrationBean.setListener(requestListener);
        return servletListenerRegistrationBean;
    }

    /**
     * 注入 ServerEndpointExporter
     * @return
     */
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }

}
