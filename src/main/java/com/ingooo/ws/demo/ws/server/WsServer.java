package com.ingooo.ws.demo.ws.server;

import com.ingooo.ws.util.WsUtil;
import com.ingooo.ws.util.config.HttpSessionConfigurator;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

/**
 * Create by 丶TheEnd on 2019/10/25 0025.
 * @author Administrator
 */
@Component
@ServerEndpoint(value = "/wsSocket", configurator = HttpSessionConfigurator.class)
public class WsServer {

    /**
     * 连接建立成功回调方法
     * @param session
     */
    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
        System.out.println("onOpen");
        boolean result = true;
        HttpSession httpSession = WsUtil.getHttpSession(config);
        if (httpSession == null) {
            result = false;
        }
        boolean online = WsUtil.isOnlineByHttpSession(httpSession);
        if (online) {
            result = false;
        }
        WsUtil.loginInit(httpSession, session);
        System.out.println(WsUtil.getHttpSessionModelByWsSession(session).getSession().getId());
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session) {
        WsUtil.removeByWsSession(session);
    }

    /**
     * 收到客户端消息后调用的方法
     * @param message
     * @param session
     */
    @OnMessage
    public void onMessage(String message, Session session) {

    }

    /**
     * 发生错误后调用的方法
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {

    }


}
